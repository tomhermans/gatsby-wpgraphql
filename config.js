const config = {
  //wordPressUrl: `https://cms.jamstack.be/wp/`, // VULTR
  wordPressUrl: 'https://oto.staging.worldofdigits.com/',
  // wordPressUrl: `http://192.168.0.240:8082/wordpress/`, // PI
  language: `EN`,
  siteRss: '/rss.xml',
  googleAnalyticsID: 'UA-42068444-1',
  menuLinks: [
    {
      name: 'Me',
      link: '/me/',
    },
    {
      name: 'Articles',
      link: '/blog/',
    },
    {
      name: 'Contact',
      link: '/contact/',
    },
  ],
  themeColor: '#3F80FF', // Used for setting manifest and progress theme colors.
  backgroundColor: '#ffffff',
}

module.exports = config
