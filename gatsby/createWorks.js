const path = require(`path`)
module.exports = async ({ actions, graphql }) => {
  const GET_WORKS = `
  query GET_WORKS($first:Int $after:String){
    wpgraphql {
      works(
        first: $first
        after:$after
      ) {
        pageInfo {
          endCursor
          hasNextPage
        }
        nodes {
          id
          uri
          workId
          title
        }
      }
    }
  }
  `
  const { createPage } = actions
  const allWorks = []
  const worksPages = []
  let pageNumber = 0
  const fetchPosts = async variables =>
    await graphql(GET_WORKS, variables).then(({ data }) => {
      const {
        wpgraphql: {
          works: {
            nodes,
            pageInfo: { hasNextPage, endCursor },
          },
        },
      } = data

      const nodeIds = nodes.map(node => node.workId)
      const worksTemplate = path.resolve(`./src/templates/works.js`)
      const worksPagePath = !variables.after ? `/` : `/page/${pageNumber}`

      worksPages[pageNumber] = {
        path: worksPagePath,
        component: worksTemplate,
        context: {
          ids: nodeIds,
          pageNumber: pageNumber,
          hasNextPage: hasNextPage,
        },
        ids: nodeIds,
      }
      nodes.map(work => {
        allWorks.push(work)
      })
      if (hasNextPage) {
        pageNumber++
        return fetchPosts({ first: 8, after: endCursor })
      }
      return allWorks
    })

  await fetchPosts({ first: 8, after: null }).then(allWorks => {
    const workTemplate = path.resolve(`./src/templates/work.js`)

    worksPages.map(worksPage => {
      console.log(`create WorkPage: ${worksPage.context.pageNumber}`)
      createPage(worksPage)
    })

    allWorks.map(work => {
      console.log(`create work: ${work.uri}`)
      createPage({
        path: `/works/${work.uri}/`,
        component: workTemplate,
        context: work,
      })
    })
  })
}
