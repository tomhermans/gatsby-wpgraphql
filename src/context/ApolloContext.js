import ApolloClient from 'apollo-boost';

export const client = new ApolloClient({
  // uri: 'http://192.168.0.240:8082/wordpress/graphql',
  uri: `https://cms.jamstack.be/wp/graphql`, // VULTR
  // uri: `http://192.168.0.240:8082/wordpress/graphql`, // PI
})

