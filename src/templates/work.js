import React, { Fragment } from "react"
import { graphql } from "gatsby"
import { Row, Col, Divider, Tag } from "antd"
import SiteLayout from "../components/SiteLayout"
import CategoriesWidget from "../components/CategoriesWidget"
import RecentCommentsWidget from "../components/RecentCommentsWidget"
import RecentPostsWidget from "../components/RecentPostsWidget"
import PostEntryMeta from "../components/PostEntryMeta"
import Seo from "../components/Seo"

const renderTermNodes = (nodes, title) => (
  <div>
    {title}
    {` `}
    {nodes.map(term => (
      <Tag>{term.name}</Tag>
    ))}
  </div>
)

const renderTerms = (categoryNodes = [], tagNodes = []) => (
  <Fragment>
    <Divider />
    {categoryNodes ? renderTermNodes(categoryNodes, `Categories: `) : null}
    {tagNodes && tagNodes.length ? renderTermNodes(tagNodes, `Tags: `) : null}
  </Fragment>
)

const Work = props => {
  const {
    location,
    data: {
      wpgraphql: { work },
    },
  } = props
  const { title, content } = work
  return (
    <SiteLayout location={location}>
      <Seo title={`${work.title}`} />
      <Row type="flex" gutter={24}>
        <Col xs={24} md={16}>
          <span>templates / work.js</span>
          <h1>{title}</h1>
          <Divider />
          <Row type="flex" justify="space-around" gutter={24}>
            <Col xs={24} md={6}>
              <PostEntryMeta post={work} />
            </Col>
            <Col xs={24} md={18}>
              <div dangerouslySetInnerHTML={{ __html: content }} />
              {work.categories.nodes.length || work.tags.nodes.length
                ? renderTerms(work.categories.nodes, work.tags.nodes)
                : null}
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={8}>
          <RecentPostsWidget />
          <CategoriesWidget />
          <RecentCommentsWidget />
        </Col>
      </Row>
    </SiteLayout>
  )
}

export default Work

export const pageQuery = graphql`
  query GET_WORK($id: ID!) {
    wpgraphql {
      work(id: $id) {
        title
        content
        uri
        author {
          name
          slug
          avatar {
            url
          }
        }
        tags {
          nodes {
            name
            link
          }
        }
        categories {
          nodes {
            name
            link
          }
        }
      }
    }
  }
`
