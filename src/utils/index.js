import config from "../../config"

export const createLocalLink = url => {
  console.log(config.wordPressUrl, ' - URL = ', url)

  if (`#` === url) {
    return null
  }
  return url.replace(config.wordPressUrl, ``)
}
