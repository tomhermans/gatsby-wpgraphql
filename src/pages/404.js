import React from "react"

import SiteLayout from "../components/SiteLayout"
import Seo from "../components/Seo"

const NotFoundPage = ({ location }) => (
  <SiteLayout location={{ location }}>
    <Seo title="404: Not found" />
    <span>pages / 404.js</span>
    <h1>NOT FOUND</h1>
    <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
  </SiteLayout>
)

export default NotFoundPage
