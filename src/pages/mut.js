import React, { useState } from "react"

import SiteLayout from "../components/SiteLayout"
import SiteHeader from "../components/SiteHeader"
import Layout from "../components/Layout"

import Seo from "../components/Seo"
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { async } from "q"

const CONTACT_MUTATION = gql`
  mutation CreateSubmissionMutation($clientMutationId: String!, $firstName: String!, $lastName: String!, $job: String!, $message: String!) {
    createSubmission(input: { clientMutationId: $clientMutationId, firstName: $firstName, job: $job, lastName: $lastName, message: $message }) {
      data
      success
    }
  }
`

const myMutPage = () => {

  const [firstNameValue, setFirstNameValue] = useState('')
  const [lastNameValue, setLastNameValue] = useState('')
  const [jobValue, setJobValue] = useState('')
  const [messageValue, setMessageValue] = useState('')

  return (
    <SiteLayout location={location}>

      <h1>Mutation test</h1>
      <p>Welcome to your new mutation page.</p>
      <p>Now go build something great. - sends but errors now.</p>

      <Mutation mutation={CONTACT_MUTATION}>
        {(createSubmission, { loading, error, data }) => (
          <React.Fragment>
            <form
              onSubmit={async event => {
                event.preventDefault()
                createSubmission({
                  variables: {
                    clientMutationId: 'example',
                    firstName: firstNameValue,
                    lastName: lastNameValue,
                    job: jobValue,
                    message: messageValue
                  }
                })
              }}
            >
              <label htmlFor="firstNameInput">First Name: </label>
              <input id='firstNameInput' type={firstNameValue}
                onChange={event => {
                  setFirstNameValue(event.target.value)
                }}
              />

              <br />
              <label htmlFor="lastNameInput">Last Name: </label>
              <input id='lastNameInput' type={lastNameValue}
                onChange={event => {
                  setLastNameValue(event.target.value)
                }}
              />

              <br />
              <label htmlFor="jobInput">Job: </label>
              <input id='jobInput' type={jobValue}
                onChange={event => {
                  setJobValue(event.target.value)
                }}
              />

              <br />
              <label htmlFor="messageInput">Message: </label>
              <input id='messageInput' type={messageValue}
                onChange={event => {
                  setMessageValue(event.target.value)
                }}
              />

              <br />
              <button type="submit">Send it to WP</button>

            </form>

            <div style={{ marginTop: '20px', padding: '20px', border: '2px solid rebeccapurple' }}>
              {loading && <p>Loading ...</p>}
              {error && <p>Error has occured ...</p>}
              {data && <p>Alrighty !! works. check WP ...</p>}
            </div>
          </React.Fragment>

        )}
      </Mutation>

    </SiteLayout>
  )
}

export default myMutPage
