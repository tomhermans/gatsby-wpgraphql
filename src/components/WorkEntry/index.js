import React, { Fragment } from "react"
import { Link, graphql } from "gatsby"
import { Row, Col, Divider } from "antd"
import config from "../../../config"
import WorkEntryMeta from "../WorkEntryMeta"

const WorkEntry = ({ work }) => {
  return (
    <Fragment>
      <Row type="flex" justify="space-around" gutter={16}>
        <Col xs={24} md={4}>
          <WorkEntryMeta work={work} />
        </Col>
        <Col xs={24} md={20}>
          <h2>
            <Link to={`/works/${work.uri}`}>{work.title}</Link>
          </h2>
          <div
            dangerouslySetInnerHTML={{
              __html: work.content.replace(config.wordPressUrl, ``),
            }}
          />
        </Col>
      </Row>
      <Divider />
    </Fragment>
  )
}

export default WorkEntry

export const query = graphql`
  fragment WorkEntryFragment on WPGraphQL_Work {
    id
    title
    uri
    slug
    date
    content: excerpt
    author {
      name
      slug
      avatar(size: 50) {
        url
      }
    }
  }
`
