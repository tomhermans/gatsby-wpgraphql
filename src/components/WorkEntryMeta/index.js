import React from 'react'
import moment from "moment/moment"
import { Row, Col, Avatar } from "antd"
import { Link } from "gatsby"

const WorkEntryMeta = ({ work }) => (
  <Row
    justify="middle"
    style={{ textAlign: `center`, marginBottom: `15px`, fontSize: `12px` }}
    gutter={16}
  >
    <Col xs={24} md={24} style={{ textAlign: `center` }}>
      <Link to={`/author/${work.author.slug}`}>
        <Avatar size={50} src={work.author.avatar.url} />
      </Link>
    </Col>
    <Col xs={24} md={24} style={{ textAlign: `center` }}>
      <Link to={`/author/${work.author.slug}`}>{work.author.name}</Link>
      <br />
      {moment(work.date).format(`MMM Do YY`)}
    </Col>
  </Row>
)

export default WorkEntryMeta
