import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { createLocalLink } from "../../utils"


const QUERY = graphql`
  {
    wpgraphql {
      posts(first: 5) {
        nodes {
          id
          title
          link
        }
      }
    }
  }
`

const RecentPostsWidget = () => (
  <StaticQuery
    query={QUERY}
    render={data => {
      return (
        <div>
          <h2>Recent Posts</h2>
          <ul>
            {data.wpgraphql.posts.nodes.map(post => {

              if (post.link.includes("209.250.241.82")) {
                return (
                  <li>
                    <Link to={createLocalLink(post.link)}>{post.title}</Link>
                  </li>
                )
              } else {
                return (
                  <li>
                    <a href="{post.link}">{post.title}</a>
                  </li>
                )
              }


            })}
          </ul>
        </div>
      )
    }}
  />
)

export default RecentPostsWidget
