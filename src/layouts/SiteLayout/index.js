import React from 'react'
import Transition from '../components/transition'


const SiteLayout = ({ location }) => {
  <Transition location={location}>
    {children}
  </Transition>
}
